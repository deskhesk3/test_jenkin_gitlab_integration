# Use the official Nginx image from Docker Hub
FROM nginx:latest

# Copy static website files into the container
COPY ./html /usr/share/nginx/html

# Expose port 80
EXPOSE 80
